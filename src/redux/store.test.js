import { goodsReducer } from './goodsReducer';
import { cartReducer } from './cartReducer';
import { favoritesReducer } from './favoritesReducer';
import { fanReducer } from './fanReducer';

describe('intReducers', () => {
    test('should return the initial state', () => {
        expect(goodsReducer(false, { type: undefined })).toEqual(false);
    });

    test('should return the initial state', () => {
        expect(cartReducer([], { type: undefined })).toEqual([]);
    });

    test('should return the initial state', () => {
        expect(favoritesReducer([], { type: undefined })).toEqual([]);
    });

    test('should return the initial state', () => {
        expect(fanReducer(false, { type: 'MOD_TO_CART' })).toEqual(true);
    });
});
