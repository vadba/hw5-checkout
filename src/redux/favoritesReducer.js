const defaultState = [];

export function favoritesReducer(state = defaultState, action) {
    switch (action.type) {
        case 'TO_FAVORITES':
            return state.find(card => card.art === action.payload.good.art) ? state.filter(good => good.art !== action.payload.good.art) : [...state, action.payload.good];
        case 'FROM_FAVORITES':
            return state.filter(good => good !== action.payload.good);
        default:
            return state;
    }
}
