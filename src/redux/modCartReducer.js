const defaultState = [];
export function modCartReducer(state = defaultState, action) {
    switch (action.type) {
        case 'MOD_CART':
            return action.payload.good;
        default:
            return state;
    }
}
