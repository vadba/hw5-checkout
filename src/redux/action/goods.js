import { request } from '../../tools/request';

export function cardsAsync() {
    return async function (dispatch) {
        const { res, err } = await request();
        dispatch({
            type: 'GET_CARDS',
            payload: {
                cards: res,
            },
        });
    };
}
