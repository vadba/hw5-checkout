const defaultState = [];
export function cartReducer(state = defaultState, action) {
    switch (action.type) {
        case 'TO_CART':
            return state.find(card => card.art === action.payload.good.art) ? state : [...state, action.payload.good];
        case 'FROM_CART':
            return state.filter(good => good !== action.payload.good);
        case 'CLEAR':
            return [];
        default:
            return state;
    }
}
