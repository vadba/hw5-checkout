const defaultState = false;
export function modReducer(state = defaultState, action) {
    switch (action.type) {
        case 'OPEN_MOD':
            return (state = true);
        case 'CLOSE_MOD':
            return (state = false);
        default:
            return state;
    }
}
