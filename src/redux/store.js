import { applyMiddleware, createStore } from 'redux';
import { combineReducers } from 'redux';
import { favoritesReducer } from './favoritesReducer';
import { cartReducer } from './cartReducer';
import { fanReducer } from './fanReducer';
import { goodsReducer } from './goodsReducer';
import { modReducer } from './modReducer';
import { modCartReducer } from './modCartReducer';
import { composeEnhancers, middleware } from '../middleware';

export const intReducer = combineReducers({
    goods: goodsReducer,
    favorites: favoritesReducer,
    cart: cartReducer,
    fan: fanReducer,
    mod: modReducer,
    modCart: modCartReducer,
});

export const store = createStore(intReducer, composeEnhancers(applyMiddleware(...middleware)), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
