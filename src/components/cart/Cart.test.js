import { render } from '@testing-library/react';
import Cart from './Cart';
import { store } from '../../redux/store';
import { Provider } from 'react-redux';

describe('Cart component', () => {
    test('snapshot', () => {
        expect(
            render(
                <Provider store={store}>
                    <Cart />
                </Provider>
            )
        ).toMatchSnapshot();
    });
});
