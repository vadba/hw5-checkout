import s from './Cart.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import Checkout from '../checkout';
import { ReactComponent as StarIconOn } from '../../assets/starOn.svg';

function Cart() {
    const dispatch = useDispatch();

    const selectCart = store => {
        return store.cart;

        // return store.goods.filter(good => store.favorites.includes(good.id));
    };

    const carts = useSelector(selectCart);

    const deleteCart = good => {
        dispatch({
            type: 'MOD_CART',
            payload: {
                good: good,
            },
        });
        dispatch({
            type: 'MOD_FROM_CART',
        });
        dispatch({
            type: 'OPEN_MOD',
        });
    };

    const fromFavoritesClick = () => {};

    const renderCards = cart => (
        <li key={cart.art} className={s.cart}>
            <span className={s.deleteItem} id={cart.art} onClick={() => deleteCart(cart)}>
                &times;
            </span>
            <span
                className={s.starIconOn}
                id={cart.art}
                onClick={() => {
                    fromFavoritesClick(cart);
                }}
            >
                <StarIconOn />
            </span>
            <img className={s.cartImg} src={cart.picUrl} alt='picture' />
            <div className={s.title}>{cart.title}</div>
            <p className={s.price}>{cart.price}</p>
        </li>
    );

    return (
        <>
            <div className={s.obg}>{carts.map(cart => renderCards(cart))}</div>
            {!!carts.length && <Checkout />}
        </>
    );
}

export default Cart;
