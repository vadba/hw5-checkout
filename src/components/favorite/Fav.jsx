import { useDispatch, useSelector } from 'react-redux';
import s from './Fav.module.scss';
import { ReactComponent as StarIconOn } from '../../assets/starOn.svg';

function Fav() {
    const dispatch = useDispatch();

    const selectFavorite = store => {
        return store.favorites;

        // return store.goods.filter(good => store.favorites.includes(good.id));
    };

    const favorites = useSelector(selectFavorite);

    const fromFavorites = good => ({
        type: 'FROM_FAVORITES',
        payload: {
            good,
        },
    });
    const fromFavoritesClick = good => {
        dispatch(fromFavorites(good));
    };

    const renderFavs = fav => (
        <li key={fav.art} className={s.cart}>
            <span
                className={s.starIconOn}
                id={fav.art}
                onClick={() => {
                    fromFavoritesClick(fav);
                }}
            >
                <StarIconOn />
            </span>
            <img className={s.cartImg} src={fav.picUrl} alt='picture' />
            <div className={s.title}>{fav.title}</div>
            <p className={s.price}>{fav.price}</p>
            <button className={s.toCart} onClick={() => fav}>
                TO CART
            </button>
        </li>
    );

    return <div className={s.obloj}>{favorites.map(fav => renderFavs(fav))}</div>;
}

export default Fav;
