import { render } from '@testing-library/react';
import Fav from './Fav';
import { store } from '../../redux/store';
import { Provider } from 'react-redux';

describe('Fav component', () => {
    test('snapshot', () => {
        expect(
            render(
                <Provider store={store}>
                    <Fav />
                </Provider>
            )
        ).toMatchSnapshot();
    });
});
