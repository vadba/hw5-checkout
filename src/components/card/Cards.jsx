import s from './Cards.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { ReactComponent as StarIcon } from '../../assets/star.svg';
import { ReactComponent as StarIconOn } from '../../assets/starOn.svg';
import { useContext } from 'react';
import { ContextStore } from '../../context';

function Cards() {
    const { toTable, handleList } = useContext(ContextStore);
    console.log(toTable);
    const selectGoods = store => {
        return store.goods;
    };

    let goods = [];

    const resGoods = useSelector(selectGoods);
    if (resGoods !== false) {
        goods = resGoods;
    }

    const dispatch = useDispatch();

    const selectFavorite = store => {
        return store.favorites;

        // return store.goods.filter(good => store.favorites.includes(good.id));
    };

    const favorites = useSelector(selectFavorite);

    const toFavorite = good =>
        dispatch({
            type: 'TO_FAVORITES',
            payload: {
                good: good,
            },
        });

    const toCart = good => {
        dispatch({
            type: 'MOD_CART',
            payload: {
                good: good,
            },
        });
        dispatch({
            type: 'MOD_TO_CART',
        });
        dispatch({
            type: 'OPEN_MOD',
        });
    };

    const liCards = card => (
        <li key={card.art} className={s.card}>
            <img className={s.cardImg} src={card.picUrl} alt='picture' />
            <div className={s.title}>{card.title}</div>
            <div className='stars' onClick={() => toFavorite(card)}>
                {favorites && favorites.find(fCard => fCard.art === card.art) ? (
                    <>
                        <StarIconOn />
                        <StarIconOn />
                        <StarIconOn />
                        <StarIconOn />
                        <StarIconOn />
                    </>
                ) : (
                    <>
                        <StarIcon />
                        <StarIcon />
                        <StarIcon />
                        <StarIcon />
                        <StarIcon />
                    </>
                )}
            </div>
            <div className={s.textCard}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus quo eligendi quam optio.</div>
            <div className={s.price_to_cart}>
                <p className={s}>{card.price}</p>
                <button className={s.toCart} onClick={() => toCart(card)}>
                    TO CART
                </button>
            </div>
        </li>
    );

    const liTable = card => (
        <li key={card.art} className={s.tcard}>
            <img className={s.tcardImg} src={card.picUrl} alt='picture' />
            <div className={s.title}>{card.title}</div>
            <div className='stars' onClick={() => toFavorite(card)}>
                {favorites && favorites.find(fCard => fCard.art === card.art) ? (
                    <div className={s.flex}>
                        <StarIconOn />
                        <StarIconOn />
                        <StarIconOn />
                        <StarIconOn />
                        <StarIconOn />
                    </div>
                ) : (
                    <div className={s.flex}>
                        <StarIcon />
                        <StarIcon />
                        <StarIcon />
                        <StarIcon />
                        <StarIcon />
                    </div>
                )}
            </div>
            <div className={s.textCard}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus quo eligendi quam optio.</div>
            <p className={s.price}>{card.price}</p>
            <div className={s.wrapToCart}>
                <button className={s.totCart} onClick={() => toCart(card)}>
                    TO CART
                </button>
            </div>
        </li>
    );

    return (
        <>
            <button className={s.toList} onClick={() => handleList()}>
                {toTable ? 'Таблиця' : 'Картки'}
            </button>
            {goods.map(card => (toTable ? liCards(card) : liTable(card)))}
        </>
    );
}

export default Cards;
