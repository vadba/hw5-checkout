import { render } from '@testing-library/react';
import Cards from './Cards';
import { store } from '../../redux/store';
import { Provider } from 'react-redux';

describe('Card component', () => {
    test('snapshot', () => {
        expect(
            render(
                <Provider store={store}>
                    <Cards />
                </Provider>
            )
        ).toMatchSnapshot();
    });
});
