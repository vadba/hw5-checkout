import { render } from '@testing-library/react';
import Checkout from './Checkout';
import { store } from '../../redux/store';
import { Provider } from 'react-redux';

describe('Cart component', () => {
    test('snapshot', () => {
        expect(
            render(
                <Provider store={store}>
                    <Checkout />
                </Provider>
            )
        ).toMatchSnapshot();
    });
});
