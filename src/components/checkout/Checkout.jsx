import style from './Checout.module.scss';
import { useFormik } from 'formik';
import { initialValues, validationSchema } from '../../tools/utils';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import s from '../cart/Cart.module.scss';
const Checkout = () => {
    const selectCart = store => {
        return store.cart;
    };
    const dispatch = useDispatch();
    const carts = useSelector(selectCart);
    const onSubmit = values => {
        setCheckout(true);
        console.log(carts, values);
        dispatch({
            type: 'CLEAR',
        });
    };

    const form = useFormik({
        initialValues,
        onSubmit,
        validationSchema,
    });

    const [make, setMake] = useState(false);
    const [checkout, setCheckout] = useState(false);

    return (
        <>
            {!make && (
                <button className={style.make} onClick={() => setMake(true)}>
                    Make
                </button>
            )}
            {make && (
                <div className={style.wrap}>
                    <form className={style.form} onSubmit={form.handleSubmit}>
                        <input type='text' name='firstName' placeholder='First Name' className={style.input} onChange={form.handleChange} value={form.values.firstName} />
                        <p className={style.errors}>{form.errors.firstName}</p>

                        <input type='text' name='secondName' placeholder='Second Name' className={style.input} onChange={form.handleChange} value={form.values.secondName} />
                        <p className={style.errors}>{form.errors.secondName}</p>

                        <input type='number' name='age' placeholder='Age' className={style.input} onChange={form.handleChange} value={form.values.age} />
                        <p className={style.errors}>{form.errors.age}</p>

                        <input
                            type='tel'
                            // autocomplete='off'
                            placeholder='phoneNumber'
                            className={style.input}
                            name='phoneNumber'
                            onChange={form.handleChange}
                            value={form.values.phoneNumber}
                        />
                        <p className={style.errors}>{form.errors.phoneNumber}</p>

                        <input type='text' placeholder='Address' className={style.input} name='address' onChange={form.handleChange} value={form.values.address} />
                        <p className={style.errors}>{form.errors.address}</p>

                        <button className={style.submit}>Checkout</button>
                    </form>
                </div>
            )}
        </>
    );
};

export default Checkout;
