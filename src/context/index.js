import { createContext, useState } from 'react';

export const ContextStore = createContext({});

const СontextProvider = ({ children }) => {
    const [toTable, setToTable] = useState(true);
    const handleList = () => {
        setToTable(!toTable);
    };

    return (
        <ContextStore.Provider
            value={{
                toTable,
                handleList,
            }}
        >
            {children}
        </ContextStore.Provider>
    );
};

export default СontextProvider;
