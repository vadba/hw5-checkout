import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Cards from '../components/card/Cards';
import Cart from '../components/cart';
import Fav from '../components/favorite';

const Router = () => {
    return (
        <Routes>
            <Route path='/' exact element={<Cards />} />
            <Route
                path='/cart'
                element={
                    <div>
                        <Cart />
                    </div>
                }
            />
            <Route
                path='/favorites'
                element={
                    <div>
                        <Fav />
                    </div>
                }
            />
        </Routes>
    );
};

export default Router;
