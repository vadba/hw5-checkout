import logo from './logo.svg';
import './App.scss';
import Button from './ui/button/Button';
import Modal from './ui/modal/Modal';
import List from './components/list/List';
import { ReactComponent as CartIcon } from './assets/cart.svg';
import { ReactComponent as FavIcon } from './assets/fav.svg';
import { Link, NavLink } from 'react-router-dom';
import Router from './routes/router';
import { useDispatch, useSelector } from 'react-redux';
import { cardsAsync } from './redux/action/goods';

function App() {
    const dispatch = useDispatch();
    dispatch(cardsAsync());
    const selectGood = store => {
        return store.modCart;
    };

    const modCart = useSelector(selectGood);

    const setCart = () => {
        dispatch({
            type: 'TO_CART',
            payload: {
                good: modCart,
            },
        });
        dispatch({
            type: 'CLOSE_MOD',
        });
    };

    const deleteCart = () => {
        dispatch({
            type: 'FROM_CART',
            payload: {
                good: modCart,
            },
        });
        dispatch({
            type: 'CLOSE_MOD',
        });
    };

    const selectFavorite = store => {
        return store.favorites;

        // return store.goods.filter(good => store.favorites.includes(good.id));
    };

    const favorites = useSelector(selectFavorite);

    const selectCart = store => {
        return store.cart;
    };

    const cart = useSelector(selectCart);

    const selectFan = store => {
        return store.fan;
    };

    const fan = useSelector(selectFan);

    const closeMod = () => {
        dispatch({
            type: 'CLOSE_MOD',
        });
    };

    return (
        <div className='App'>
            <Modal header='TO Cart' closeButton={true} text='Lorem text' backgroundColor='yellow' backgroundColorTitle='green'>
                <Button className={'buttonfirst'} text='OK' onClick={fan ? setCart : deleteCart} backgroundColor='orange' />
                <Button className={'buttonfirst'} text='Cancel' backgroundColor='orange' onClick={closeMod} />
            </Modal>
            <div className='App-conainer'>
                <header className='App-header'>
                    <div className='first-header'>
                        <img src={logo} className='App-logo' alt='logo' />
                        <div className='header-reg-cart'>
                            <div className='link-register'>Login / Register</div>

                            <div className='hover-cart'>
                                {cart.length !== 0 ? <span className='int-cart'>{cart.length}</span> : ''}
                                <CartIcon />
                            </div>

                            <div className='hover-fav'>
                                {favorites.length !== 0 ? <span className='int-fav'>{favorites.length}</span> : ''}
                                <FavIcon />
                            </div>
                        </div>
                    </div>
                    <div className='second-header'>
                        <NavLink to='/' className='nav-link'>
                            <h4>Продукты</h4>
                        </NavLink>
                        <NavLink to='/cart' className='nav-link'>
                            <h4>Cart</h4>
                        </NavLink>
                        <NavLink to='/favorites' className='nav-link'>
                            <h4>Favorites</h4>
                        </NavLink>
                    </div>
                </header>
                <main className='App-main'>
                    <List>
                        <Router />
                    </List>
                </main>
            </div>
        </div>
    );
}

export default App;
