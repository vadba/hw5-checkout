import * as Yup from 'yup';

export const initialValues = {
    firstName: '',
    secondName: '',
    age: '',
    address: '',
    phoneNumber: '',
};

export const validationSchema = Yup.object().shape({
    firstName: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Required'),
    secondName: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Required'),
    address: Yup.string().required('Required'),
    phoneNumber: Yup.string().min(13, 'Too Short!').max(15, 'Too Long!').required('Required'),
    age: Yup.number().min(1, 'Too Short!').max(115, 'Too Long!').required('Required'),
});
