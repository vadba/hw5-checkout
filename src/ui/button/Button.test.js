import { render, screen } from '@testing-library/react';
import Button from './Button';

describe('Button component', () => {
    test('InTheDocument', () => {
        render(<Button className='btn-primary'>Button</Button>);
        expect(screen.getByText('Button')).toBeInTheDocument();
    });
    test('props -- className', () => {
        render(<Button className='btn-primary'>Button</Button>);
        expect(screen.getByText('Button')).toHaveClass('btn-primary');
    });
    test('props -- type', () => {
        render(<Button className='btn-primary'>Button</Button>);
        expect(screen.getByText('Button')).toHaveAttribute('type', 'button');
    });
    test('props -- children', () => {
        render(<Button className='btn-primary'>Button</Button>);
        expect(screen.getByText('Button')).not.toBeNull();
    });
});
