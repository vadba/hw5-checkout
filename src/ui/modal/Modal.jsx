import s from './Modal.module.scss';
import cn from 'classnames';
import { useDispatch, useSelector } from 'react-redux';

function Modal(props) {
    const {
        header = 'Do you want to delete?',
        closeButton = false,
        text = 'Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?',
        children,
        backgroundColor,
        backgroundColorTitle,
    } = props;

    const selectMod = store => {
        return store.mod;

        // return store.goods.filter(good => store.favorites.includes(good.id));
    };

    const mod = useSelector(selectMod);

    const dispatch = useDispatch();

    const closeMod = () => {
        dispatch({
            type: 'CLOSE_MOD',
        });
    };

    return (
        <>
            {mod && (
                <>
                    <div className={s.overlay} onClick={closeMod}></div>
                    <div className={cn(s.openModal, s[backgroundColor])}>
                        {closeButton && (
                            <button className={s.closeButton} onClick={closeMod}>
                                &times;
                            </button>
                        )}
                        <h4 className={cn(s.modalTitle, s[backgroundColorTitle])}>{header}</h4>
                        <div className={s.modalContent}>
                            <p className={s.modalText}>{text}</p>
                            <div className={s.buttons}>{children}</div>
                        </div>
                    </div>
                </>
            )}
        </>
    );
}

export default Modal;
