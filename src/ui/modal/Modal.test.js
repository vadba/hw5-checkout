import { render, screen } from '@testing-library/react';
import Modal from './Modal';
import { Provider, useDispatch } from 'react-redux';
import { store } from '../../redux/store';

describe('Modal component', () => {
    test('noModal', () => {
        const { container } = render(
            <Provider store={store}>
                <Modal />
            </Provider>
        );
        const noModal = container.firstChild;
        expect(noModal).toBeNull();
    });
});
